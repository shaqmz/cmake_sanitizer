Steps to reproduce in repo dir:
- mkdir build && cd build
- cmake ..
- make
- ctest --output-on-failure -D ExperimentalMemCheck