
cmake_minimum_required(VERSION 3.0)

project(cmake_sanitizer LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 11)

#set(MEMORYCHECK_COMMAND "")
set(MEMORYCHECK_TYPE "AddressSanitizer")
set(MEMORYCHECK_SANITIZER_OPTIONS "verbosity=1:symbolize=1:abort_on_error=1:detect_leaks=1")

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fsanitize=address")
set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -fsanitize=address")
set(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} -fsanitize=address")
#add_compile_options("-fsanitize=address")
#add_link_options("-fsanitize=address")

add_executable(app main.cpp)

include(CTest)
enable_testing()

add_library(Catch INTERFACE)
target_include_directories(Catch INTERFACE catch)

add_executable(tests tests.cpp)
target_link_libraries(tests PUBLIC Catch)

add_test(NAME mytests COMMAND tests)